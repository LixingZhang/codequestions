<?php
/**
 * Created by PhpStorm.
 * User: Lixing
 * Date: 18/2/18
 * Time: 6:56 PM
 */
use CodeQuestion\CodeQuestions;
use CodeQuestion\Cat;

class CodeQuestionsTest extends \PHPUnit\Framework\TestCase
{
    public function testStartsWithUppercase()
    {
        $codeQuestion = new CodeQuestions();
        $string = 'ABC';
        $this->assertTrue($codeQuestion->startsWithUppercase($string));
        $string = 'Abc';
        $this->assertTrue($codeQuestion->startsWithUppercase($string));

        $string = 'abc';
        $this->assertFalse($codeQuestion->startsWithUppercase($string));

        $string = '1abc';
        $this->assertFalse($codeQuestion->startsWithUppercase($string));
    }

    public function testCircleArea()
    {
        $codeQuestion = new CodeQuestions();
        $this->assertInternalType('float', $codeQuestion->circleArea(5));
        $this->assertNotEquals(0, $codeQuestion->circleArea(5));
        $this->assertEquals(78.5398163397, $codeQuestion->circleArea(5));
        $this->assertEquals(28.2743338823, $codeQuestion->circleArea(3));
    }

    public function testExtractWordsNumbers()
    {
        $string = 'One day 10 questions: 2 on each day, for 5 weeks.';
        $codeQuestion = new CodeQuestions();
        $result = $codeQuestion->extractWordsNumbers($string);

        $this->assertContains('One', $result['words']);
        $this->assertContains('day', $result['words']);
        $this->assertContains('questions', $result['words']);
        $this->assertContains('on', $result['words']);
        $this->assertContains('each', $result['words']);
        $this->assertContains('day', $result['words']);
        $this->assertContains('for', $result['words']);
        $this->assertContains('weeks', $result['words']);
        $this->assertCount(8, $result['words']);
        $this->assertNotContains('hello', $result['words']);

        $this->assertContains('10', $result['numbers']);
        $this->assertContains('2', $result['numbers']);
        $this->assertContains('5', $result['numbers']);
        $this->assertNotContains('8', $result['numbers']);
        $this->assertCount(3, $result['numbers']);
    }

    public function testContainsWithIn()
    {
        $haystack = 'Some text goes in here with a lot of wørds to $search through. We\'ll add more words if we want.';
        $codeQuestion = new CodeQuestions();
        $needle = 'search';
        $this->assertTrue($codeQuestion->containsWithIn($haystack, $needle));
        $needle = 'some';
        $this->assertTrue($codeQuestion->containsWithIn($haystack, $needle));

        $needle = 'well';
        $this->assertFalse($codeQuestion->containsWithIn($haystack, $needle));
        $needle = 'words';
        $this->assertTrue($codeQuestion->containsWithIn($haystack, $needle));
        $needle = 'word';
        $this->assertTrue($codeQuestion->containsWithIn($haystack, $needle));
    }

    public function testQuestion9()
    {
        $players1 = array('M. Sharapova','N. Djøvick','A. Murray','V. Williams');
        $players2 = array('Djovick','Williams, V','Williams, S','S, Williams','Murray','Sharapova');
        $codeQuestion = new CodeQuestions();
        $expect = <<<TEXT
M. Sharapova is also known as Sharapova
N. Djøvick is also known as Djovick
A. Murray is also known as Murray
V. Williams is also known as Williams, V

TEXT;

        $this->expectOutputString($expect);
        $codeQuestion->question9($players1, $players2);
    }

    public function testQuestion92()
    {
        $players1 = array('M. Sharapova','N. Djøvick','A. Murray','V. Williams');
        $players2 = array('Djovick','Williams, V','Williams, S','S, Williams','Murray','Sharapova');
        $codeQuestion = new CodeQuestions();
        $expect = <<<TEXT
Djovick is also known as N. Djøvick
Williams, V is also known as V. Williams
Williams, S is also known as V. Williams
S, Williams is also known as V. Williams
Murray is also known as A. Murray
Sharapova is also known as M. Sharapova

TEXT;

        $this->expectOutputString($expect);
        $codeQuestion->question9($players2, $players1);
    }

    public function testQuestion12()
    {
        $codeQuestion = new CodeQuestions();
        $this->expectOutputRegex('/Sha Tin: Feels like 45 \(last updated 2014-07-07 23:56pm\)/');
        $codeQuestion->question12();
    }

    public function testQuestion13()
    {
        $codeQuestion = new CodeQuestions();
        $codeQuestion->question13();
    }

}