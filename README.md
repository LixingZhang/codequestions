##Description
Answer for question 11 is in ```src/myClass.php``` file

Answer for question 5 is in ```src/Cat.php``` file

Answers for the rest questions are in ```src/CodeQuestions.php``` file

##Setup
```
git clone git@bitbucket.org:LixingZhang/codequestions.git
cd codequestions/
composer install
```

##Running Test
```
./vendor/bin/phpunit tests/CodeQuestionsTest.php
```