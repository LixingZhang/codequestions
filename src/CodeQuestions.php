<?php

// Using PHP7+
// Please add any relevant citations that assisted you in the answering of questions - these can be from any source on the internet or real world.
// If you use online run-time tools (such as phpfiddle.org) a link to these as the answer will suffice, otherwise just write them inline. Please
// do not use a Framework as part of your answers.
declare(strict_types = 1);
namespace CodeQuestion;
/*
  Question 1:
  Write a function that determines if a string starts with an upper-case letter A-Z.
*/

/**
 * Matches if a string start with an upper-case letter A-Z
 *
 * @param string $string
 *
 * @return bool
 */
class CodeQuestions
{

    function startsWithUppercase(string $string): bool
    {
        if (preg_match('/^[A-Z].*/', $string)) {
            return true;
        }

        return false;
    }


    /*
      Question 2:
      Write a function that determines the area of a circle given the radius.
    */

    /**
     * Get the area of a circle given the radius
     *
     * @param float $radius
     *
     * @return float
     */
    function circleArea(float $radius): float
    {
        return M_PI * pow($radius, 2);
    }

    /*
      Question 3:
      Write a function that can sum all of the values in any given array.
    */

    function sum(array $array): float
    {
        return array_sum($array);
    }


    /*
      Question 4:
      What does "extends" do to the following "Cat" class?

      class Cat extends Animal
      {

      }
    */

    function question4()
    {
        $answer = 'The Cat class inherits all of the public and protected methods and properties from the Animal class. Unless the Cat class overrides those methods, they will retain their original functionality.';
    }


    /*
        Question 6:

       Provide examples for the different loops in native PHP, write tests in which the code block executes exactly 32 times

    */
    function question6(): void
    {
        //foreach loop, 32 times
        $array = range(0, 31);
        foreach ($array as $value) {
            echo $value . PHP_EOL;
        }

        //while loop, 32 times
        $count = 0;
        while ($count < 32) {
            echo $count . PHP_EOL;
            $count++;
        }

        //do while loop, 32 times
        $count = 0;
        Do {
            echo $count . PHP_EOL;
            $count++;
        } while ($count < 32);

        //for loop, 32 times
        for ($count = 0; $count < 32; $count++) {
            echo $count . PHP_EOL;
        }
    }

    /*
      Question 7:

      Given the following input:
      'One day 10 questions: 2 on each day, for 5 weeks.'

      Write simple logic in PHP code that can extract both the words and numbers and put them into their own arrays, '$words', '$numbers'.

    */

    /**
     * @param string $input
     *
     * @return array
     */
    function extractWordsNumbers(string $input): array
    {
        $words = [];
        $numbers = [];

        if (preg_match_all('/\b\w+\b/', $input, $matches)) {
            foreach ($matches[0] as $match) {
                if (is_numeric($match)) {
                    $numbers[] = $match;
                    continue;
                }

                if (is_string($match)) {
                    $words[] = $match;
                    continue;
                }
            }
        }

        return compact('words', 'numbers');
    }

    /*
        Question 8:

      Write a function that tests for a certain substring and returns a boolean to indicate whether or not the substring is present.
      The search should be case-insensitive and match accented characters to the english equivalent.

        // Example
      $needle = 'search';
        $haystack = 'Some text goes in here with a lot of wørds to $search through. We\'ll add more words if we want.';
        $wasFound = containsWithin($haystack, $needle);

      // Be sure to try 'some', 'search', 'well', 'words', and 'word'.

    */

    /**
     * @param string $haystack
     * @param string $needle
     * @link http://www.mendoweb.be/blog/php-replace-accented-characters/
     * @return bool
     */
    function containsWithIn(string $haystack, string $needle): bool
    {
        $search = explode(
            ",",
            "ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ"
        );
        $replace = explode(
            ",",
            "c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE"
        );
        $haystack = str_replace($search, $replace, $haystack);

        if (preg_match_all("/{$needle}/i", $haystack)) {
            return true;
        }

        return false;
    }

    /*
      Question 9:

        Write a function to 'best match' players based on how close their names are.

        <?php
            // The Data
            $players1 = array('M. Sharapova','N. Djøvick','A. Murray','V. Williams');
            $players2 = array('Djovick','Williams, V','Williams, S','S, Williams','Murray','Sharapova');
        ?>
        M. Sharapova is also known as Sharapova
      N. Djøvick is also known as Djovick
      V. Williams is also known as Williams, V

      This function should also work the other way around if you switched the inputs
      Williams, V is also known as V. Williams
    */

    /**
     * There is some confusing in this question.
     * I am not sure how we defined the names are `close` to each other.
     * Does it have to be the same name or just be close on the the spelling?
     *
     * @param array $players1
     * @param array $players2
     *
     * @link http://php.net/manual/en/function.metaphone.php
     * @link http://php.net/manual/en/function.levenshtein.php
     */
    function question9(array $players1, array $players2): void
    {
        foreach ($players1 as $player1) {

            $shortest = -1;
            foreach ($players2 as $player2) {
                // calculate the distance between the input word,
                // and the current word
                $lev = min(levenshtein(soundex($player1), soundex($player2)), levenshtein(metaphone($player1), metaphone($player2)));
                // check for an exact match
                if ($lev == 0) {
                    $closest = $player2;
                    // break out of the loop; we've found an exact match
                    break;
                }

                // if this distance is less than the next found shortest
                // distance, OR if a next shortest word has not yet been found
                if ($lev < $shortest || $shortest < 0) {
                    // set the closest match, and shortest distance
                    $shortest = $lev;
                    $closest = $player2;
                }
            }

            if (isset($closest)) {
                echo sprintf('%s is also known as %s', $player1, $closest) . PHP_EOL;
            }
        }
    }

    /*
        Question 10:

      What is wrong here?  No code is required here, just a short commented response.

      class myClass implements theClass {
        private $_foo;

        public function __construct() {
          $this->_foo = 1;
        }

        public static function getMyFoo() {
          return $this->_foo * 5;
        }
      }
    */

    /**
     * @link http://php.net/manual/en/language.oop5.static.php
     */
    function question10()
    {
        $answer = 'Because static methods are callable without an instance of the object created, the pseudo-variable $this is not available inside the method declared as static.';

    }


    /*
        Question 12:

      consider file_get_contents("http://dnu5embx6omws.cloudfront.net/venues/weather2.json");
      Parse the data into a PHP format, sort the content from highest to lowest 'feels like' temperature, then output them in this format (using system-appropriate line breaks):

      Adelaide River: Feels like 41 (last updated 2014-08-10 12:00pm)
      Flemington: Feels like 39 (last updated 2014-08-10 7:00am)
      Randwick: Feels like 37 (last updated 2014-08-10 1:00pm)

    */

    /**
     *
     */
    function question12(): void
    {
        $weather = file_get_contents("http://dnu5embx6omws.cloudfront.net/venues/weather2.json");
        $data = json_decode($weather, true)['data'];

        $data = array_filter(
            $data,
            function ($venue) {
                return isset($venue['weatherFeelsLike']);
            }
        );
        usort(
            $data,
            function ($a, $b) {

                if ((int)$a['weatherFeelsLike'] == (int)$b['weatherFeelsLike']) {
                    return 0;
                };

                return ((int)$a['weatherFeelsLike'] < (int)$b['weatherFeelsLike']) ? 1 : -1;
            }
        );

        foreach ($data as $venue) {
            echo sprintf(
                     '%s: Feels like %s (last updated %s)',
                     $venue['name'],
                     $venue['weatherFeelsLike'],
                     date('Y-m-d H:ia', $venue['weatherLastUpdated'])
                 ) . PHP_EOL;
        }
    }


    /*
        Question 13:

      consider file_get_contents("http://dnu5embx6omws.cloudfront.net/venues/weather2.json");
      Parse the JSON data into a PHP format, and demonstrate a simple php script populating the relevant data into the provided MySQL schema which keeps the data rows updated or created;

      CREATE TABLE `codetest` (
      `VenueID`  int(11) NOT NULL AUTO_INCREMENT,
      `Name`  varchar(64) NULL,
      `WeatherCondition`  varchar(255) NULL,
      `Temperature`  tinyint(4) NULL,
      `FeelsLike`  tinyint(4) NULL,
      `LastUpdated`  datetime NULL,
      PRIMARY KEY (`VenueID`)
      );

    */

    /**
     *
     */
    function question13(): void
    {
        $weather = file_get_contents("http://dnu5embx6omws.cloudfront.net/venues/weather2.json");
        $data = json_decode($weather, true)['data'];
        try {
            //update username and password here
            $user = 'root';
            $password = '';
            $dbh = new \PDO('mysql:host=localhost;dbname=test', $user, $password);
            foreach ($data as $venue) {
                $sql = 'SELECT COUNT(*) FROM codetest WHERE VenueID = :venueID';
                $sth = $dbh->prepare($sql);
                $sth->bindParam(':venueID', $venue['venueId'], \PDO::PARAM_INT);
                $sth->execute();
                $result = $sth->fetchColumn();
                if (!isset($venue['name'])) {
                    $venue['name'] = null;
                }
                if (!isset($venue['weatherCondition'])) {
                    $venue['weatherCondition'] = null;
                }
                if (!isset($venue['weatherTemp'])) {
                    $venue['weatherTemp'] = null;
                }
                if (!isset($venue['weatherFeelsLike'])) {
                    $venue['weatherFeelsLike'] = null;
                }
                if (!isset($venue['weatherLastUpdated'])) {
                    $venue['weatherLastUpdated'] = null;
                } else {
                    $venue['weatherLastUpdated'] = date('Y-m-d H:i:s', $venue['weatherLastUpdated']);
                }


                if ($result > 0) {
                    //update
                    $sql = 'UPDATE codetest SET Name = :name, WeatherCondition = :weatherCondition, Temperature = :temperature, FeelsLike = :feelsLike, LastUpdated = :lastUpdated WHERE VenueID = :venueID';
                    $sth = $dbh->prepare($sql);
                    $sth->bindParam(':name', $venue['name'], \PDO::PARAM_STR);
                    $sth->bindParam(':weatherCondition', $venue['weatherCondition'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(':temperature', $venue['weatherTemp'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(':feelsLike', $venue['weatherFeelsLike'], \PDO::PARAM_INT | \PDO::PARAM_NULL);
                    $sth->bindParam(':lastUpdated', $venue['weatherLastUpdated'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(':venueID', $venue['venueId'], \PDO::PARAM_INT);
                    $sth->execute();
                } else {
                    //create
                    $sql = 'INSERT INTO codetest (VenueID, Name, WeatherCondition, Temperature, FeelsLike, LastUpdated) VALUES (?, ?, ?, ?, ?, ?)';
                    $sth = $dbh->prepare($sql);
                    $sth->bindParam(1, $venue['venueId'], \PDO::PARAM_INT);
                    $sth->bindParam(2, $venue['name'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(3, $venue['weatherCondition'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(4, $venue['weatherTemp'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(5, $venue['weatherFeelsLike'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->bindParam(6, $venue['weatherLastUpdated'], \PDO::PARAM_STR | \PDO::PARAM_NULL);
                    $sth->execute();
                }
            }

        } catch (\PDOException $e) {
            print "Error!: " . $e->getMessage();
            die();
        }
    }


    /*
      Question 14:

      Using your codetest table, demonstrate only the sql query that provides;

      1) The venue name and weather condition with the highest temperature.
      2) How many days ago was the oldest row updated?
      3) Which venue has the greatest difference in 'feels like' and actual temperature?
    */

    function question14()
    {
        $sql1 = "SELECT `Name`, `WeatherCondition`, `Temperature` FROM `codetest` WHERE Temperature = (SELECT MAX(`Temperature`) FROM `codetest`);";
        $sql2 = "SELECT DATEDIFF(NOW(), (SELECT MIN(`LastUpdated`) FROM `codetest`)) AS 'DAYS AGO';";
        $sql3 = "SELECT * FROM `codetest` WHERE ABS(`Temperature` - `FeelsLike`) = (SELECT MAX(ABS(`Temperature` - `FeelsLike`)) FROM `codetest`);";
    }


    /*
      Last Question :)

      Write a script to read the front page of www.punters.com.au and echo out, for each of today's featured races:
      - Race name
      - Prize money

    */

    function lastQuestion(): void
    {
        $ch = curl_init();
        $timeout = 5;
        $url = 'https://www.punters.com.au/';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);

        $features = [];
        if ($data) {
            $dom = new \DOMDocument();
            @$dom->loadHTML($data);
            $todaysFeatures = $dom->getElementById('todaysfeatures');
            $nodes = $todaysFeatures->childNodes;
            foreach ($nodes as $node) {
                if ($node instanceof \DOMElement && $node->tagName == 'div' && $node->getAttribute(
                        'class'
                    ) == 'ModuleContent') {
                    foreach ($node->childNodes as $childNode) {
                        if ($childNode instanceof \DOMElement && $childNode->tagName == 'div' && $childNode->getAttribute(
                                'class'
                            ) == 'component-wrapper form-guide-feature-races') {
                            foreach ($childNode->childNodes as $child) {
                                if ($child instanceof \DOMElement && $child->tagName == 'a' && $child->getAttribute(
                                        'class'
                                    ) == 'js-event-link') {
                                    $featureEvent = $child->firstChild->nextSibling;
                                    $feature = [];
                                    foreach ($featureEvent->childNodes as $event) {
                                        if ($event instanceof \DOMElement) {
                                            $isToday = false;
                                            if (strpos(
                                                    $event->getAttribute('class'),
                                                    'feautred-event__time'
                                                ) !== false) {
                                                foreach ($event->childNodes as $eventChild) {
                                                    if ($eventChild instanceof \DOMElement && $eventChild->tagName == 'abbr') {
                                                        $now = new \DateTime();
                                                        $tomorrow = $now->modify('+1 day')->setTime(0, 0, 0);
                                                        if ($eventChild->getAttribute(
                                                                'data-utime'
                                                            ) < $tomorrow->getTimestamp()) {
                                                            $isToday = true;
                                                        }
                                                    }
                                                }
                                            }

                                            $feature['isToday'] = $isToday;
                                            if ($event->getAttribute('class') == 'feautred-event__name') {
                                                foreach ($event->childNodes as $eventChild) {
                                                    if ($eventChild instanceof \DOMText && !empty(
                                                        trim(
                                                            $eventChild->textContent
                                                        )
                                                        )) {
                                                        $feature['name'] = trim($eventChild->textContent);
                                                    }

                                                    if ($eventChild instanceof \DOMElement && $eventChild->getAttribute(
                                                            'class'
                                                        ) == 'feautred-event__details') {
                                                        $feature['prize'] = trim($eventChild->textContent);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    $features[] = $feature;
                                }
                            }
                        }
                    }
                }

            }
        }

        foreach ($features as $feature) {
            if ($feature['isToday']) {
                echo 'Race Name: ' . $feature['name'] . PHP_EOL;
                echo 'Prize Money: ' . $feature['prize'] . PHP_EOL;
            }
        }
    }

}
