<?php
/**
 * Created by PhpStorm.
 * User: Lixing
 * Date: 18/2/18
 * Time: 7:22 PM
 */
namespace CodeQuestion;
/*
     Question 5:
     Create a function called "purr()" in the "Cat" class (above).
     Echo 'purr' every x seconds with a specified delay to start purring
   */

class Animal
{

}

class Cat extends Animal
{

    public function purr(int $delay): void
    {
        while (true) {
            echo 'purr' . PHP_EOL;
            sleep($delay);
        }
    }
}
