<?php
namespace CodeQuestion;
/*
Question 11:

How would you fix the above example considering the following interface definition constraint?

interface theClass {
public function __construct();
public static function getMyFoo();
}

*/

interface theClass
{

    public function __construct();

    public static function getMyFoo();
}

class myClass implements theClass
{

    private $_foo;

    public function __construct()
    {
        $this->_foo = 1;
    }

    public static function getMyFoo()
    {
        return (new self())->_foo * 5;
    }
}